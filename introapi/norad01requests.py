import requests

def main():
    satellites = requests.get("http://www.celestrak.com/NORAD/elements/active.txt")

    satellites = satellites.text.split("\r\n")

    satellitenames=[]
    f = open("satellites_names.txt", "w+")
    i =0
    for satellite in satellites:
        if i%3==0:
            satellite = satellite.strip()
            if satellite is not "":
                satellitenames.append(satellite)
        i+=1


    for s in satellitenames:
        print(s, file = f)
    print(f'\nThe number of satellites tracked is {len(satellitenames)}')
    f.close()


if __name__ == "__main__":
    main()
