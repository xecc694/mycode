#!/usr/bin/python3
"""
Author: RZFeeser
This program harvests SpaceX data avail from https://api.spacexdata.com/v3/cores using the requests library.
"""

# python3 -m pip install requests
import requests
from colorama import init, Fore, Back, Style

# GLOBAL / CONSTANT of the API we want to lookup
SPACEXURI = "https://api.spacexdata.com/v3/cores"

def main():
    # create a requests response object by sending an HTTP GET to SPACEXURI
    coreData = requests.get(SPACEXURI)

    # Pull JSON off 200 and convert to lists and dictionaries
    listOfCores = coreData.json()

    for core in listOfCores:
        print(core)
        print(f"The {Fore.GREEN}core serial {Fore.WHITE}of this launch is {core['core_serial']}")
        print(f"The {Fore.BLUE}core details {Fore.WHITE}of this launch are: ", end="")
        if core['details'] is None:
            print(f"{Fore.RED}{core['details']}{Fore.WHITE}")
        else:
            print(f"{core['details']}")
        print(f"The {Fore.YELLOW}original details {Fore.WHITE}of this launch are: {core['original_launch']}")
        print()

if __name__ == "__main__":
    main()

