#!/usr/bin/env python3

import netifaces

# An interface is an object that is configured on a machine for the use of network communication.

'''
A network interface is the point of interconnection between a computer and a private or public network. A network interface is generally a network interface card (NIC), but does not have to have a physical form. Instead, the network interface can be implemented in software. For example, the loopback interface (127.0.0.1 for IPv4 and ::1 for IPv6) is not a physical device but a piece of software simulating a network interface. The loopback interface is commonly used in test environments.

https://docs.oracle.com/javase/tutorial/networking/nifs/definition.html
'''

print(netifaces.interfaces())

for i in netifaces.interfaces():
    print(i)
    print('\n**************Details of Interface - ' + i + ' *********************')
    #print(netifaces.ifaddresses(i))
    #print(netifaces.ifaddresses(i)[netifaces.AF_LINK])
    try:
        print("MAC: ", end ='')
        print((netifaces.ifaddresses(i)[netifaces.AF_LINK])[0]['addr']) # Prints the MAC address
        print("IP: ", end ='')
        print((netifaces.ifaddresses(i)[netifaces.AF_INET])[0]['addr']) # Prints the IP address
    except:
        print("Could not collect adapter information.")
