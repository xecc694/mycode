#!/usr/bin/env python3
## USOpen Tournament Switch Checker -- 2018.05.01
''' usopen.py
This script is being designed to provide the following automated tasks:
- Ping check the router (import os)
- Login check the router (import netmiko)
- Determine if interfaces in use are up (import netmiko)
- Apply new configuration (import netmiko)

The IPs and device type should be made available via a spreadsheet

'''
import os

## ADD THE LINE BELOW THIS COMMENT
import bootstrapper

## pyexcel and pyexce-xls are required for our program to execute
# python3 -m pip install --user pyexcel
# python3 -m pip install --user pyexcel-xls
import pyexcel

# python3 -m pip install --user netmiko
from netmiko import ConnectHandler

## retrieve data set from spreadsheet
def retv_excel(par):
    d = {}
    records = pyexcel.iget_records(file_name=par) # create a record object that is an open spreadsheet
    for record in records:
         # adds a new hostname and driver key:value pair to our dictionary
        d.update( { record['hostname'] : record['driver'] } )
    return d # return the completed dictionary

## Ping router - returns True or False
def ping_router(hostname):

    response = os.system("ping -c 1 " + hostname)

    #and then check the response...
    if response == 0:
        return True
    else:
        return False

## Check interfaces - Issue "show ip init brief"
def interface_check(dev_type, dev_ip, dev_un, dev_pw):
    try:
        open_connection = ConnectHandler(device_type=dev_type, ip=dev_ip, username=dev_un, password=dev_pw)
        my_command = open_connection.send_command("show management api http-commands")       
    except:
        my_command = "** ISSUING COMMAND FAILED **"
    finally:
        return my_command


## Login to router - SSH Check with Netmiko class ConnectHandler
def login_router(dev_type, dev_ip, dev_un, dev_pw):
    try:
        open_connection = ConnectHandler(device_type=dev_type, ip=dev_ip, username=dev_un, password=dev_pw)
        return True        
    except:
        return False

## Main function - This is the code that runs our functions
def main():

    ## Determine where *.csv input is
    file_location = input("\nWhere is the file location? ")

    ## Entry is now a local dictionary containing hostname(key):driver(value)
    entry = retv_excel(file_location)

   
    for x in entry.keys():
        print(interface_check(entry[x], x, "admin", "alta3"))
                
        


## Call main()
if __name__ == "__main__":
    main()

